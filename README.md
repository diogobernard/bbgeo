# Application

The application starts with the following details.

## Dependency

- Java 8
- Maven
- Docker

## Run:
```
mvn clean install
docker build -t bbgeo . 
docker run -p 0.0.0.0:8080:8080 -d -e GOOGLE_KEY="[YOUR_GOOGLE_API_KEY]" --name bbgeo bbgeo
```


## resquest

for example:

```

curl -X GET \
  'http://localhost:8080/geo/search?address=2+Abbey+Rd,+London+NW8+0AH,+UK'

```