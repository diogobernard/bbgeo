package br.com.diogo.bbgeo;

import static org.junit.Assert.*;

import org.apache.camel.CamelContext;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.MockEndpoints;
import org.apache.camel.test.spring.UseAdviceWith;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;


@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = Application.class)
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@MockEndpoints
@UseAdviceWith
public class RouteTest {

    protected Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    CamelContext context;

    @Test
    public void testRouterunning() throws Exception {
        context.start();
        assertTrue(context.getRouteStatus("search-api").isStarted());
        context.stop();
    }
}
