package br.com.diogo.bbgeo;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.xmljson.XmlJsonDataFormat;
import org.apache.camel.model.rest.RestParamType;
import org.springframework.stereotype.Component;
import java.util.Arrays;

@Component
public class Route extends RouteBuilder {

    public void configure() throws Exception {

        XmlJsonDataFormat xmlJsonFormat = new XmlJsonDataFormat();
        xmlJsonFormat.setForceTopLevelObject(true);
        xmlJsonFormat.setEncoding("UTF-8");
        xmlJsonFormat.setTrimSpaces(true);
        xmlJsonFormat.setRootName("GeocodeResponse");
        xmlJsonFormat.setSkipNamespaces(true);
        xmlJsonFormat.setRemoveNamespacePrefixes(true);
        xmlJsonFormat.setExpandableProperties(Arrays.asList("D", "E"));

        rest("/search").description("Address REST service")
                .get().param().name("address").type(RestParamType.query).endParam()
                .route().routeId("search-api")
                .removeHeader("accept-encoding")
                .log("param: ${headers}")
                .to("direct:google")
                .setHeader("Cache-Control", constant("no-cache"))
                .convertBodyTo(String.class,"UTF-8")
                .marshal(xmlJsonFormat)
                .setHeader("Content-Type",constant("application/json;charset=UTF-8"))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        from("direct:google")
                .routeId("google-api")
                .setHeader(Exchange.HTTP_QUERY, simple("?address=${header.address}&key={{google.api.key}}"))
                .to("{{google.map.url}}?bridgeEndpoint=true");

    }
}
