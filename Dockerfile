FROM java:8

COPY target/bbgeo.jar /usr/src/
WORKDIR /usr/src

ENTRYPOINT ["java", "-jar","bbgeo.jar"]